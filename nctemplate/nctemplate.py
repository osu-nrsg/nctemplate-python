import shlex
import shutil
import subprocess
import tempfile
import time
from contextlib import suppress
from pathlib import Path
from typing import Optional, Union

from ncdataset import NCDataset


class NCTemplateError(Exception):
    pass


class NCTemplate(NCDataset):
    """NCTemplate is used to create a template netCDF dataset either from a netCDF CDL
    file or an existing netCDF dataset.

    When used in a context manager (`with ... :`), the dataset is provided, rather than
    the NCTemplate object.
    """

    _private_atts = NCDataset._private_atts + ["_tempfile"]
    _tempfile: bool = False

    def __init__(
        self,
        template_path: Union[Path, str],
        new_nc_path: Optional[Path] = None,
        **kwargs,
    ):
        """Initialize the template dataset.

        Parameters
        ----------
        template_path : Path
            Path to an existing template NetCDF file, or a NetCDF CDL file to use to
            create the template NetCDF file.
        new_nc_path : Path, optional
            Path to generate the template NetCDF file, if template_path points to a CDL
            file. By default, the template NetCDF file is create with the same path and
            name as the CDL file with the nc extension. If template_path is a .nc file,
            this is ignored.
        **kwargs : optional
            Other arguments to pass in to netCDF4.Dataset

        Raises
        ------
        FileNotFoundError
            The template netCDF or CDL file was not found.
        ValueError
            The provided template file was not a .nc or .cdl file.
        """
        template_path = Path(template_path)
        if template_path.suffix.lower() == ".cdl":
            # Create the template NC file in the same dir as the cdl file if possible,
            # else in /tmp. Retry up to three times in case another instance of ncgen is
            # running (which causes an error)
            max_tries = 3
            for try_i in range(max_tries):
                try:
                    tmpl_nc_path = self._ncgen(template_path, new_nc_path)
                except NCTemplateError:
                    time.sleep(0.25)  # wait to try again
                    if (try_i + 1) >= max_tries:
                        raise
                else:
                    break

        elif template_path.suffix.lower() == ".nc":
            if not template_path.exists():
                raise FileNotFoundError(f"No file exists at {str(template_path)}.")
            tmpl_nc_path = template_path
        else:
            raise ValueError(
                "NCTemplate expected a path ending in .cdl or .nc. Got"
                f" {template_path.suffix}."
            )
        super().__init__(tmpl_nc_path, **kwargs)

    def __del__(self):
        """Closes the dataset and deletes a tempfile if necessary"""
        if self.isopen():
            self.close()
        if self._tempfile:
            # delete the temporary nc file
            with suppress(FileNotFoundError):
                self.nc_path.unlink()

    def _ncgen(self, cdl_path: Path, new_nc_path: Optional[Path] = None) -> Path:
        """Create a NetCDF file from a CDL file. Requires the NetCDF `ncgen` utility.

        If the NetCDF file cannot be created at the specified path, it is created as a
        tempfile.

        Parameters
        ----------
        cdl_path : Path
            Path to the CDL file to use to create the NetCDF file.
        new_nc_path : Path, optional
            Path to the new NetCDF file to create. If not provided, it is created in the
            dir with the CDL file with the same name as the CDL file.

        Returns
        -------
        Path
            Path to the created NetCDF file.

        Raises
        ------
        NCTemplateError
            If `ncgen` is missing or there is an error creating the NetCDF file.
        """
        cdl_dir = cdl_path.parent
        nc_name = f"{cdl_path.stem}.nc"
        tmpl_nc_path = new_nc_path or (cdl_dir / nc_name)
        # Make sure we can write the nc file to this spot.
        self._tempfile = False
        try:
            with open(str(tmpl_nc_path), "w") as fobj:
                fobj.write("\0")
        except OSError:
            # If not, make a temporary nc file. (Silently! :O)
            tmpl_nc_path = Path(tempfile.gettempdir(), nc_name)
            self._tempfile = True
        # Remove an existing template NC file if it exists.
        with suppress(FileNotFoundError):
            tmpl_nc_path.unlink()

        ncgen_cmd = f'ncgen -b -k nc4 -o "{tmpl_nc_path}" "{cdl_path}"'
        # noinspection PyArgumentList
        result: subprocess.CompletedProcess = subprocess.run(
            shlex.split(ncgen_cmd), capture_output=True
        )
        if result.returncode != 0:
            raise NCTemplateError(
                "Error generating template nc file:"
                f" {result.stderr.decode('utf8').rstrip()}"
            )
        # tmpl_nc_path now points to the template NetCDF file
        return tmpl_nc_path


if not shutil.which("ncgen"):
    raise NCTemplateError(
        "The ncgen tool is not present or is not in PATH on this PC. Install netcdf-bin"
        " or similar."
    )
