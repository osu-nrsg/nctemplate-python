import warnings
from contextlib import suppress

from .nctemplate import NCTemplate, NCTemplateError

__all__ = ["__version__", "NCTemplate", "NCTemplateError"]


def _get_version() -> str:
    try:
        import importlib.metadata as importlib_metadata
    except ImportError:
        try:
            import importlib_metadata
        except ImportError:
            warnings.warn(
                "importlib-metadata package needed to get the version of this package."
            )
            return "unknown"
    with suppress(importlib_metadata.PackageNotFoundError):
        return importlib_metadata.version(__name__)

    # OK, the package isn't installed. Try reading the pyproject.toml file.
    from pathlib import Path

    pp_toml: Path = Path(__file__).absolute().parent.parent / "pyproject.toml"
    if pp_toml.is_file():
        try:
            import toml
        except ImportError:
            pass
        else:
            # we have toml
            pyproject = toml.load(pp_toml)
            with suppress(KeyError):
                return pyproject["tool"]["poetry"]["version"]
    # got here if we couldn't get the version through the package metadata or TOML file.
    return "unknown"


__version__ = _get_version()
