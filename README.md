# NCTemplate

NCTemplate is a tool to create a template NetCDF4 dataset from a template NetCDF CDL file using the `ncgen`
command-line utility (must be installed on the OS).

NCTemplate is a subclass of [NCDataset](https://gitlab.com/osr-nrsg/ncdataset-python) and thus includes the
context-manager features of that class.

## Usage

```python
with NCTemplate("template.cdl") as tmpl_ds, NCDataset("new_ds.nc", mode="w") as new_ds:
    # Custom code here to copy dimensions, variables, and attributes from tmpl_ds to new_ds
    pass
```
