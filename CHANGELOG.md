# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2024-05-30

- Explicit exports in `__init__.py`
- Update for poetry 1.8.3 compat

## [1.1.0] - 2024-05-28

- Update docstrings
- Minor refactors
- Switch to ruff
- **Min python version 3.8**

## [1.0.1] - 2021-05-13

- Retry ncgen a few times if there is an error

## [1.0.0] - 2021-02-10

- ncdataset v1.0.0

## [0.1.3]

- ncdataset v0.3.0

## [0.1.2]

- Check for `ncgen` on import rather than call.

## [0.1.1] - 2021-02-08

- Raise any `OSError` from `subprocess.run` as NCTemplateError for `ncgen` not installed.

## [0.1.0] - 2020-10-08

- Initial release

[Unreleased]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v1.1.1...master
[1.1.1]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v0.1.3...v1.0.0
[0.1.3]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/osu-nrsg/nctemplate-python/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/osu-nrsg/nctemplate-python/-/tags/v0.1.0
